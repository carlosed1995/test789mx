<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name'      => 'Carlos Centeno',
            'email'     => 'ccen@gmail.com',
            'password'     => bcrypt('12345678'),
        ]);

        factory(App\User::class, 7)->create();
    }
}
