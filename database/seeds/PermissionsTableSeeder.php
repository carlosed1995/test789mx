<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Permission list
        Permission::create(['name' => 'editar-post']);
        Permission::create(['name' => 'eliminar-post']);
        Permission::create(['name' => 'comentar-post-all-user']); 

        //Admin
        $admin = Role::create(['name' => 'administrador']);

        $admin->givePermissionTo([
            'editar-post',
            'eliminar-post',
            'comentar-post-all-user', 
        ]);
        //$admin->givePermissionTo('products.index');
        //$admin->givePermissionTo(Permission::all());
       
        //Guest
        $guest = Role::create(['name' => 'publicador']);

        $guest->givePermissionTo([
            'comentar-post-all-user', 
        ]);

        //User Admin
        $user = User::find(1); //Carlos Centeno
        $user->assignRole('administrador');
    }
}
