<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Models\Comments::class, function (Faker $faker) {
    return [
        'post_id' => App\Models\Posts::all()->random()->id,
        'user_id' => User::all()->random()->id,
        'comment' => $faker->text(50),
    ];
});