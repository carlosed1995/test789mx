<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Models\Posts::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->text(50),
        'user_id' => User::all()->random()->id,
    ];
});