<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(); 
Route::resource('post', 'PostController');  
Route::resource('feed', 'FeedController');  
Route::get('profile','UsersController@index');   
Route::post('updateprofile','UsersController@updateProfile')->name('profile.update'); 
Route::post('updatecomment','FeedController@updateComment')->name('updatecomment'); 
