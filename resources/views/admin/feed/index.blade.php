@extends('layouts.layout')

@section('content')

<div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
              @if(Session::has('message')) 
                <p class="alert alert-success">{{ Session::get('message') }}</p>
              @endif
              @if(Session::has('error-message')) 
                <p class="alert alert-danger">{{ Session::get('error-message') }}</p>
              @endif
                <h3 class="card-title">Mis post</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="table" class="table table-bordered table-striped">
                 <thead>
                  <tr>
                   <th>#</th>
                   <th>Titulo</th>
                   <th></th>
                   </tr>
                 </thead>
                 <tbody>
                 @foreach($posts as $key => $post)
                  <tr>
                  <td>{{$key +1}}</td>
                  <td>{{$post->title}}</td>
                  <td>
                   <div class="container">
                    <div class="row">

                    <a href="{{route('feed.show',$post->id)}}" class="col-lg-2 btn btn-warning load "><i class="fas fa-eye"></i></a>
                    @can('editar-post')
                     <a href="{{route('feed.edit',$post->id)}}" class="col-lg-2 btn btn-info load "><i class="fas fa-edit"></i></a>
                     @endcan
                     @can('eliminar-post')
                     <a onclick="event.preventDefault();document.getElementById('delete-post-{{$post->id}}').submit();"  class="col-lg-2 btn btn-danger load "><i class="fas fa-trash"></i></a>
                     <form action="{{ route('post.destroy', $post->id) }}" id="delete-post-{{$post->id}}" method="POST">
                       @method('DELETE')
                       @csrf 
                     </form>
                     @endcan
                    </div>
                   </div>
                  </td>
                 </tr>
                 @endforeach
                 </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
 
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

@endsection