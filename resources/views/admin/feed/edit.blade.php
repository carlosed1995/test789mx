@extends('layouts.layout')

@section('content')
          <div class="card card-primary">
            <div class="card-header">
             <h3 class="card-title">Editar Feed post</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start --> 
            <form method="POST" role="form" action="{{route('feed.update',$post->id)}}">
            {{csrf_field()}}
            @method('PUT')
            <div class="card-body">
             <div class="form-group">
                <label for="title">Titulo</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Ingrese titulo" value="{{ $post->title }}" required>
            </div>
            @error('title') 
             <div class="alert alert-danger">{{ $message }}</div>
            @enderror
             <div class="form-group">
              <label for="content">Contenido</label> 
              <textarea class="form-control" rows="3" id="content" name="content" placeholder="Ingrese contenido" required>{{ $post->content }}</textarea>
             </div>
            @error('content') 
             <div class="alert alert-danger">{{ $message }}</div>
            @enderror  
           <div class="card-footer">
            <button type="submit" class="btn btn-primary">Enviar datos</button>
           </div>
          </form>
          <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Comentarios</h3>
              </div>
              <div class="card-body" id="dinamic-comment">
              @foreach($comments as $key => $comment)
              @if($comment->user_id == Auth::user()->id)
              <div class="row mt-3 div-parent">
                  <div class="col-10">
                  <textarea class="form-control" rows="2" placeholder="Ingrese comentario" disabled>{{$comment->comment}}</textarea>
                  <i>Autor: {{$comment->getUser->name}}</i>
                  </div>
                  <div class="col-2">
                  <a href="{{ route('feed.destroy', $comment->id) }}" onclick="event.preventDefault();document.getElementById('delete-comment-{{$comment->id}}').submit();"  class="btn btn-danger pull-right"><i class="fas fa-trash"></i>
                  </a>
                     <form action="{{ route('feed.destroy', $comment->id) }}" id="delete-comment-{{$comment->id}}" method="POST">
                       @method('DELETE')
                       @csrf 
                     </form> 
                  </div>
                </div>
                @else
                @can('editar-post') 
                <div class="row mt-3 div-parent">
                  <div class="col-10">
                  <textarea class="form-control" rows="2" placeholder="Ingrese comentario" disabled>{{$comment->comment}}</textarea>
                  <i>Autor: {{$comment->getUser->name}}</i>
                  </div>
                  <div class="col-2">
                  <a href="{{ route('feed.destroy', $comment->id) }}" onclick="event.preventDefault();document.getElementById('delete-comment-{{$comment->id}}').submit();"  class="btn btn-danger pull-right"><i class="fas fa-trash"></i>
                  </a>
                     <form action="{{ route('feed.destroy', $comment->id) }}" id="delete-comment-{{$comment->id}}" method="POST">
                       @method('DELETE')
                       @csrf 
                     </form> 
                  </div>
                </div>
                @else
                <div class="row mt-3">
                  <div class="col-12">
                  <textarea class="form-control" rows="2" name="" placeholder="Ingrese comentario" disabled>{{$comment->comment}}</textarea>
                  <i>Autor: {{$comment->getUser->name}}</i>
                  </div>
                </div>

                @endcan

                @endif
                
              @endforeach
              <br>
              </div>
             
            </div>  
         </div>
         </div>


@endsection
 