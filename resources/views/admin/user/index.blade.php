@extends('layouts.layout')

@section('content')
              @if(Session::has('message')) 
                <p class="alert alert-success">{{ Session::get('message') }}</p>
              @endif
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Perfil del usuario</h3>
              </div>
              <form method="post" role="form" action="{{route('profile.update')}}">
              {{csrf_field()}} 
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Nombre completo</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese nombre completo" value="{{ $user->name }}" required>
                  </div>
                  @error('name') 
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="name">email</label> 
                    <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese nombre completo" value="{{ $user->email }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="birthday">Fecha de nacimiento</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{ $user->birthday }}">
                  </div>
                  @error('birthday') 
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="nationality">Nacionalidad</label>
                    <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Ingrese nacionalidad" value="{{ $user->nationality }}">
                  </div>
                  @error('nationality') 
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>


@endsection