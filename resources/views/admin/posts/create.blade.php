@extends('layouts.layout')

@section('content')
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Crear post</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start --> 
              <form method="post" role="form" action="{{route('post.store')}}">
              {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Titulo</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Ingrese titulo" value="{{ old('title') }}" required>
                  </div>
                  @error('title') 
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="content">Contenido</label> 
                    <textarea class="form-control" rows="3" id="content" name="content" placeholder="Ingrese contenido" required>{{ old('content') }}</textarea>
                  </div>
                  @error('content') 
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>


@endsection