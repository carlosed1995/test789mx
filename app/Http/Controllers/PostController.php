<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $posts = Posts::where('user_id', '=', Auth::user()->id)->get();
        return view('admin.posts.index', ['posts' => $posts]);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custom_validation_messages = array(
            'title.required' => "El titulo es requerido.",
            'title.min' => "El titulo no puede tener menos de 3 caracteres.",
            'content.required' => "El contenido es requerido.",
            'content.min' => "El contenido no puede tener menos de 4 caracteres."
          );

          $validator = Validator::make($request->all(), [
            "title" => 'required|min:3',
            "content" => 'required|min:4'

        ],$custom_validation_messages);

        if ($validator->fails()) {
            return back()
                   ->withErrors($validator)
                   ->withInput();
        }

         
        $post =  Posts::create([
            'title' => $request->title,
            'content' => $request->content,
            'user_id' => Auth::user()->id
        ]); 

        Session::flash('message', 'Post creado');
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $post =  Posts::find($id);
        return view('admin.posts.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $custom_validation_messages = array(
            'title.required' => "El titulo es requerido.",
            'title.min' => "El titulo no puede tener menos de 3 caracteres.",
            'content.required' => "El contenido es requerido.",
            'content.min' => "El contenido no puede tener menos de 4 caracteres."
          );

          $validator = Validator::make($request->all(), [
            "title" => 'required|min:3',
            "content" => 'required|min:4'

        ],$custom_validation_messages);

        if ($validator->fails()) {
            return back()
                   ->withErrors($validator)
                   ->withInput();
        }

         
        $post =  Posts::find($id);
        $post->title = $request->title;
        $post->content = $request->content; 
        $post->save(); 
    

        Session::flash('message', 'Post editado');
        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post =  Posts::find($id);
        if($post){
            Comments::where('post_id',$id)->delete();
            $post->delete();
            Session::flash('message', 'Post eliminado');
            return redirect()->route('post.index');
        }else{
            Session::flash('error-message', 'Post no encontrado');
            return redirect()->route('post.index');
        }
        return redirect()->back();


    }
}
