<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Comments; 
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;


class FeedController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::orderBy('created_at', 'desc')->get();
        return view('admin.feed.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post =  Posts::find($id);
        $comments = Comments::where('post_id',$id)->get();
        return view('admin.feed.show', ['post' => $post, 'comments' => $comments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->can('editar-post')){
        $post =  Posts::find($id);
        $comments = Comments::where('post_id',$id)->get();
        return view('admin.feed.edit', ['post' => $post, 'comments' => $comments]);
        }else{
            Session::flash('error-message', 'No tiene permiso para ingresar en esta sección.');
            return redirect()->route('feed.index');

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $custom_validation_messages = array(
            'title.required' => "El titulo es requerido.",
            'title.min' => "El titulo no puede tener menos de 3 caracteres.",
            'content.required' => "El contenido es requerido.",
            'content.min' => "El contenido no puede tener menos de 4 caracteres."
          );

          $validator = Validator::make($request->all(), [
            "title" => 'required|min:3',
            "content" => 'required|min:4'

        ],$custom_validation_messages);

        if ($validator->fails()) {
            return back()
                   ->withErrors($validator)
                   ->withInput();
        } 


        if(auth()->user()->can('editar-post')){  
        $post =  Posts::find($id);
        $post->title = $request->title;
        $post->content = $request->content; 
        $post->save(); 

        }

        if(isset($request->comment)){
            Comments::create([
                'post_id' => $id,
                'comment' => $request->comment,
                'user_id' => Auth::user()->id,
               ]); 
        }

         
        Session::flash('message', 'Cambios realizados');
        return redirect()->route('feed.index');

        
   
    }
 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment =  Comments::find($id);
        if($comment){ 
            $comment->delete();
            Session::flash('message', 'Comentario eliminado');
            return redirect()->back();
        }else{
            Session::flash('error-message', 'Comentario no encontrado');
            return redirect()->back();
        }
        return redirect()->back();
    }
}
