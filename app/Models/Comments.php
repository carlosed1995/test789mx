<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'post_id', 'user_id', 'comment'
    ];

    public function getUser(){    
        return $this->hasOne('App\User','id', 'user_id');
    }
}
